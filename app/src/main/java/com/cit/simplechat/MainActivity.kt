package com.cit.simplechat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import org.java_websocket.client.WebSocketClient
import org.java_websocket.handshake.ServerHandshake
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Exception
import java.net.URI

class MainActivity : AppCompatActivity() {

    private lateinit var adapter: ChatAdapter
    private lateinit var client: WebSocketClient
    private var nowOpen: Boolean = false

    private val retrofit = Retrofit.Builder()
        .baseUrl("http://strukov-artemii.online:8085/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    private val api = retrofit.create(API::class.java)

    lateinit var chat: DataChat

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = ChatAdapter(mutableListOf())

        chat_rec.apply {
            adapter = this@MainActivity.adapter
            layoutManager = LinearLayoutManager(this@MainActivity, LinearLayoutManager.VERTICAL, true)
        }
        
        send.setOnClickListener {
            val idChat = id_chat.text.toString().toInt()
            val message = message_text.text.toString()
            if (message.isNotBlank()){
                val sendMessage = SendMessage(message, idChat, false)
                val textJson = Gson().toJson(sendMessage)
                client.send(textJson)
                message_text.text.clear()
            }
        }

        open_close.setOnClickListener {
            if (!nowOpen) {
                val idChat = id_chat.text.toString().toInt()
                val idUser = id_user.text.toString().toInt()
                val token = token_text.text.toString()
                open_close.text = "Закрыть"
                createWebSocketClient(token_text.text.toString())
                client.connect()
                nowOpen = true
                loadHistory(idUser, idChat, token)
            }else{
                open_close.text = "Открыть"
                client.close()
                nowOpen = false
                adapter.clear()
            }
        }
    }

    private fun loadHistory(idUser: Int, idChat: Int, token: String) {
        val bearerToken = "Bearer $token"
        api.getChat(idChat, bearerToken).enqueue(object: Callback<ModelChat>{
            override fun onResponse(call: Call<ModelChat>, response: Response<ModelChat>) {
                val body = response.body()
                if (body == null){
                    Toast.makeText(this@MainActivity, "Body null", Toast.LENGTH_LONG).show()
                }else{
                    chat = body.chat
                    val messagesAdapter = body.messages.map {
                        it.toModelMessageAdapter(getUser(it.idUser), it.idUser == idUser)
                    }.reversed()
                    adapter.setMessages(messagesAdapter)
                }
            }

            override fun onFailure(call: Call<ModelChat>, t: Throwable) {
                Toast.makeText(this@MainActivity, t.localizedMessage ?: t.message ?: "Error", Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun createWebSocketClient(token: String) {
        client = object: WebSocketClient(URI("ws://strukov-artemii.online:8085/chat?token=$token")) {
            override fun onOpen(handshakedata: ServerHandshake?) {}

            override fun onMessage(message: String?) {
                val modelMessage: ModelMessage = Gson().fromJson(message, ModelMessage::class.java)
                if (modelMessage.idUser == null){
                    showToast(modelMessage.message)
                }else{
                    runOnUiThread {
                        val user = getUser(modelMessage.id)
                        adapter.addMessage(modelMessage.toModelMessageAdapter(user))
                    }
                }
            }

            override fun onClose(code: Int, reason: String?, remote: Boolean) {}

            override fun onError(ex: Exception?) {
                showToast(ex?.message ?: "Unknown")
            }
        }
    }

    private fun getUser(idUser: Int): ModelUser{
        if (idUser == chat.first.id) return chat.first
        if (idUser == chat.second.id) return chat.second
        throw Exception()
    }

    private fun showToast(message: String){
        runOnUiThread { Toast.makeText(this@MainActivity, message, Toast.LENGTH_LONG).show() }
    }

    override fun onPause() {
        super.onPause()
        if (::client.isInitialized)
            client.close()
    }
}