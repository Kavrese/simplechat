package com.cit.simplechat

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface API {
    @GET("chat")
    fun getChat(@Query("idChat") idChat: Int, @Header("Authorization") bearerToken: String): Call<ModelChat>
}