package com.cit.simplechat

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_message.view.*

class ChatAdapter(private val messages: MutableList<ModelMessageAdapter>): RecyclerView.Adapter<ChatAdapter.SimpleMessage>() {

    class SimpleMessage(itemView: View): RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleMessage {
        val layoutRes = if (viewType == 0) R.layout.item_message else R.layout.item_message_other
        return SimpleMessage(LayoutInflater.from(parent.context).inflate(layoutRes, parent, false))
    }

    override fun getItemCount(): Int = messages.size

    override fun onBindViewHolder(holder: SimpleMessage, position: Int) {
        val model = messages[position]
        holder.itemView.info_message.text = "${model.user.getFIO()} - ${model.datetime}"
        holder.itemView.message_item.text = model.message
    }

    override fun getItemViewType(position: Int): Int {
        val model = messages[position]
        return if(model.isYou) 0 else 1
    }

    fun addMessage(modelMessage: ModelMessageAdapter){
        messages.add(0, modelMessage)
        notifyItemInserted(0)
    }

    fun setMessages(messages: List<ModelMessageAdapter>){
        this.messages.clear()
        this.messages.addAll(messages)
        notifyDataSetChanged()
    }

    fun clear(){
        val size = messages.size
        messages.clear()
        notifyItemRangeRemoved(0, size)
    }
}